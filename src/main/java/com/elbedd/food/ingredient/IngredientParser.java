package com.elbedd.food.ingredient;

import java.util.ArrayList;
import java.util.List;

public class IngredientParser {

	private final static String INGREDIENT_SEPARATEUR = ",";
	
	private final static List<String> INGREDIENT_START_COMPONENT = List.of("(","[");
	private final static List<String> INGREDIENT_END_COMPONENT = List.of(")","]");
	private final static List<String> INGREDIENT_WORD_SEP = List.of(" ","\t");
	private final static String INGREDIENT_PERCENT = "%";
	private final static List<String> INGREDIENT_DECIMAL_SEP = List.of(",",".");
	private final static String FINAL_CHARACTER = ".";
	

	public List<Ingredient> parse(final String ingredientList) {
		List<Ingredient> ingredients = new ArrayList<Ingredient>();
		parse(ingredientList, ingredients, 0);
		return ingredients;
	}
	
	protected int parse(final String ingredientList, final List<Ingredient> ingredients, int idxNested) {		
		StringBuilder currentIngredientName = new StringBuilder();
		StringBuilder word = new StringBuilder();
		Ingredient ingredient = new Ingredient();
		for (int i = idxNested; i < ingredientList.length(); i++) {
			String currentCaracter = "" + ingredientList.charAt(i);
			
			if (INGREDIENT_DECIMAL_SEP.contains(currentCaracter)) {
				// This 2 caracters can be use in 2 contexts : Decimal & syntax
				// Check if a % is user after in the same word
				// word is complete until the % information.
				int percentPosition = percentInformation(ingredientList, i, word);
				if (percentPosition != -1) {
					double percent = Double.parseDouble(word.toString()); 
					ingredient.setPercent(percent);
					i = percentPosition;
					word = new StringBuilder();
					continue;
				}
			}
			
			if (INGREDIENT_SEPARATEUR.equals(currentCaracter)
				|| INGREDIENT_END_COMPONENT.contains(currentCaracter)
					) {
				if (currentIngredientName.length() > 0 && word.length() > 0) {
					currentIngredientName.append(INGREDIENT_WORD_SEP.get(0));
				}
				currentIngredientName.append(word);
				
				ingredient.setName(currentIngredientName.toString());
				if (!ingredient.getName().equals("")) {
					ingredients.add(ingredient);	
				}
				
				if (INGREDIENT_END_COMPONENT.contains(currentCaracter)) {
					// end of recursive call.
					return i;
				}
				
				ingredient = new Ingredient();
				currentIngredientName = new StringBuilder();
				word = new StringBuilder();
				
			} else if (INGREDIENT_PERCENT.equals(currentCaracter)) {
				double percent = Double.parseDouble(word.toString());
				ingredient.setPercent(percent);
				word = new StringBuilder();
			} else if (INGREDIENT_START_COMPONENT.contains(currentCaracter)) {
				List<Ingredient> ingredientComposant = new ArrayList<Ingredient>();
				int  idx = parse(ingredientList,ingredientComposant, i + 1);
				if (ingredientComposant.size() > 0) {
					ingredient.setComposants(ingredientComposant);
				}
				i = idx;
			} else {
				if (FINAL_CHARACTER.equals(currentCaracter)) {
					// Dot : '.' for decimal done previously
					// Can be the final (only space after)
					// Or a abbreviation
					// Test that only INGREDIENT_WORD_SEP after
					if (hasOnlySpaceChar(ingredientList, i + 1)) {
						break;
					}
					
				}
				if (INGREDIENT_WORD_SEP.contains(currentCaracter)) {
					if(word.length()>0) {
						if (currentIngredientName.length() > 0) {
							currentIngredientName.append(INGREDIENT_WORD_SEP.get(0));
						}
						currentIngredientName.append(word);
						word = new StringBuilder();	
					}
				} else {
					word.append(currentCaracter);	
				}
				
			}
		}
		if (currentIngredientName.length() > 0 || word.length() > 0) {
			if (word.length() > 0) {
				if (currentIngredientName.length() > 0) {
					currentIngredientName.append(INGREDIENT_WORD_SEP.get(0));
				}
				currentIngredientName.append(word);
			}
			ingredient.setName(currentIngredientName.toString());
			ingredients.add(ingredient);
		}
		
		
		return ingredientList.length();
	}


	private boolean hasOnlySpaceChar(String ingredientList, int startChar) {
		boolean ret = true;
		for (int idx = startChar; idx < ingredientList.length(); idx++) {
			String character = "" + ingredientList.charAt(idx);
			if (!INGREDIENT_WORD_SEP.contains(character)) {
				ret = false;
				break;
			}
		}
		
		return ret;
	}

	protected int percentInformation(final String ingredientList, final int indexStringList, StringBuilder word) {
		int percentWithDecimal = -1;
		
		StringBuilder afterDecimal = new StringBuilder();
		for (int i = indexStringList + 1; i < ingredientList.length(); i++) {
			char c = ingredientList.charAt(i);
			if (c >= '0' && c <= '9') {
				afterDecimal.append(c);
			} else if (c == '%') {
				word.append(".");//decimal caracter
				word.append(afterDecimal);
				percentWithDecimal = i;
				break;
			} else {
				break;
			}

		}

		return percentWithDecimal;
	}

}

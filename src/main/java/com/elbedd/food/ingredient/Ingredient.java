package com.elbedd.food.ingredient;

import java.util.List;

public class Ingredient {

	private List<Ingredient> composants;
	
	private double percent;
	
	private String name;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public double getPercent() {
		return percent;
	}
	
	public void setPercent(double percent) {
		this.percent = percent;
	}

	public List<Ingredient> getComposants() {
		return composants;
	}
	
	public void setComposants(List<Ingredient> composants) {
		this.composants = composants;
	}
	
}

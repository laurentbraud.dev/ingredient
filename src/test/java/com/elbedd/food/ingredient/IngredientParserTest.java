package com.elbedd.food.ingredient;

import static org.junit.Assert.assertEquals;

import java.util.List;

import org.junit.Test;

public class IngredientParserTest {

	@Test
	public void parseTestSimple() {
		String ingredientList = "sucre, beurre de cacao.";
		
		IngredientParser ean = new IngredientParser();
		List<Ingredient> ingredients = ean.parse(ingredientList);
		assertEquals(2, ingredients.size());
		assertEquals("sucre", ingredients.get(0).getName());
		assertEquals("beurre de cacao", ingredients.get(1).getName());
	}
	
	@Test
	public void parseTestNoFinalDot() {
		String ingredientList = "sucre, beurre de cacao";
		
		IngredientParser ean = new IngredientParser();
		List<Ingredient> ingredients = ean.parse(ingredientList);
		assertEquals(2, ingredients.size());
		assertEquals("sucre", ingredients.get(0).getName());
		assertEquals("beurre de cacao", ingredients.get(1).getName());
	}
	
	
	@Test
	public void parseTestPercent() {
		String ingredientList = "sucre, beurre de cacao, Fleur de sel 1%, émulsifiant.";
		
		IngredientParser ean = new IngredientParser();
		List<Ingredient> ingredients = ean.parse(ingredientList);
		assertEquals(4, ingredients.size());
		assertEquals("sucre", ingredients.get(0).getName());
		assertEquals("beurre de cacao", ingredients.get(1).getName());
		assertEquals("Fleur de sel", ingredients.get(2).getName());
		assertEquals("émulsifiant", ingredients.get(3).getName());
		assertEquals(1, ingredients.get(2).getPercent(), 0);
	}
	
	@Test
	public void percentInformationTest() {
		IngredientParser ean = new IngredientParser();
		
		StringBuilder sb = new StringBuilder("0");
		int newPos = ean.percentInformation("Fleur de sel 0,3%, émulsifiant.", 14, sb);
		assertEquals("0.3", sb.toString());
		assertEquals(16, newPos);
	}
	
	@Test
	public void parseTestPercentVirgule() {
		String ingredientList = "sucre, beurre de cacao, Fleur de sel 0,3%, émulsifiant.";
		
		IngredientParser ean = new IngredientParser();
		List<Ingredient> ingredients = ean.parse(ingredientList);
		assertEquals(4, ingredients.size());
		assertEquals("sucre", ingredients.get(0).getName());
		assertEquals("beurre de cacao", ingredients.get(1).getName());
		assertEquals("Fleur de sel", ingredients.get(2).getName());
		assertEquals("émulsifiant", ingredients.get(3).getName());
		assertEquals(0.3, ingredients.get(2).getPercent(), 0);
	}
	
	@Test
	public void parseTestPercentPoint() {
		String ingredientList = "sucre, beurre de cacao, Fleur de sel 0.3%, émulsifiant.";
		
		IngredientParser ean = new IngredientParser();
		
		List<Ingredient> ingredients = ean.parse(ingredientList);
		assertEquals(4, ingredients.size());
		assertEquals("sucre", ingredients.get(0).getName());
		assertEquals("beurre de cacao", ingredients.get(1).getName());
		assertEquals("Fleur de sel", ingredients.get(2).getName());
		assertEquals("émulsifiant", ingredients.get(3).getName());
		assertEquals(0.3, ingredients.get(2).getPercent(), 0);
	}
	
	@Test
	public void parseTestComponent() {
		String ingredientList = "grains de caramel (sucre, lactose, beurre laitier concentré) 15%, arôme.";
		
		IngredientParser ean = new IngredientParser();
		List<Ingredient> ingredients = ean.parse(ingredientList);
		assertEquals(2, ingredients.size());
		assertEquals("grains de caramel", ingredients.get(0).getName());
		assertEquals(15, ingredients.get(0).getPercent(), 0);
		assertEquals("arôme", ingredients.get(1).getName());
		
		List<Ingredient> componentIngredient = ingredients.get(0).getComposants(); 
		assertEquals(3, componentIngredient.size());
		assertEquals("sucre", componentIngredient.get(0).getName());
		assertEquals("lactose", componentIngredient.get(1).getName());
		assertEquals("beurre laitier concentré", componentIngredient.get(2).getName());
		
	}
	
	@Test
	public void parseTestMultiComponent() {
		String ingredientList = "a, b (c 30%, d (e, f 4%)) 15%, g (h)";		
		
		IngredientParser ean = new IngredientParser();
		List<Ingredient> ingredients = ean.parse(ingredientList);
		assertEquals(3, ingredients.size());
		assertEquals("b", ingredients.get(1).getName());
		assertEquals(15, ingredients.get(1).getPercent(), 0);
		assertEquals("g", ingredients.get(2).getName());
		assertEquals(0, ingredients.get(2).getPercent(), 0);

		List<Ingredient> componentIngredient = ingredients.get(1).getComposants(); 
		assertEquals(2, componentIngredient.size());
		assertEquals("c", componentIngredient.get(0).getName());
		assertEquals(30, componentIngredient.get(0).getPercent(), 0);
		assertEquals("d", componentIngredient.get(1).getName());
		assertEquals(0, componentIngredient.get(1).getPercent(), 0);
		
		List<Ingredient> componentIngredient2 = componentIngredient.get(1).getComposants(); 
		assertEquals(2, componentIngredient2.size());
		assertEquals("e", componentIngredient2.get(0).getName());
		assertEquals(0, componentIngredient2.get(0).getPercent(), 0);
		assertEquals("f", componentIngredient2.get(1).getName());
		assertEquals(4, componentIngredient2.get(1).getPercent(), 0);
	}

	@Test
	public void parseIngredientsTestMultiSlash() {
		// extrait gel douche
		String ingredientList = "AQUA / WATER / EAU, SODIUM LAURETH SULFATE";		
		
		IngredientParser ean = new IngredientParser();
		List<Ingredient> ingredients = ean.parse(ingredientList);
		assertEquals(2, ingredients.size());
		assertEquals("AQUA / WATER / EAU", ingredients.get(0).getName());
		assertEquals(0, ingredients.get(0).getPercent(), 0);
	}

	@Test
	public void parseIngredientsTestDot() {
		// extrait gel douche
		String ingredientList = "EXT.D&C VIOLET NO.2 (CI 69730)";		
		
		IngredientParser ean = new IngredientParser();
		List<Ingredient> ingredients = ean.parse(ingredientList);
		assertEquals(1, ingredients.size());
		
		assertEquals("EXT.D&C VIOLET NO.2", ingredients.get(0).getName());
		assertEquals(0, ingredients.get(0).getPercent(), 0);
	}

	@Test
	public void parseIngredientsTestDotAndSlash() {
		// extrait shampoo
		String ingredientList = "PARFUM / FRAGANCE (F.I.L. C21402/1)";		
		
		IngredientParser ean = new IngredientParser();
		List<Ingredient> ingredients = ean.parse(ingredientList);
		assertEquals(1, ingredients.size());
		
		assertEquals("PARFUM / FRAGANCE", ingredients.get(0).getName());
		assertEquals(0, ingredients.get(0).getPercent(), 0);
		
		List<Ingredient> componentIngredient = ingredients.get(0).getComposants(); 
		assertEquals(1, componentIngredient.size());
		assertEquals("F.I.L. C21402/1", componentIngredient.get(0).getName());
		assertEquals(0, componentIngredient.get(0).getPercent(), 0);
	}
	
}
